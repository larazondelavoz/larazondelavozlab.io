---
layout: page
title: Acerca de
permalink: /about/
---

Ya estas cerca del borde del abismo, yo soy la voz que está justo en el borde. sin cuerpo, sin rostro, sin nombre, sin identidad, no tengo edad. Solo tengo mi voz, no es prominente y quizás despues de todo sientas decepción, pero no te preocupes, cuando comience nuestra verdadera conversación ya veras como tu perspectiva de mí cambia por completo.  

  
Este pequeño espacio fue creado gracias Juan Febles de Podcast Linux! acá como encontrarle!:
+ Twitter: <https://twitter.com/podcastlinux>
+ Correo: <podcastlinux@avpodcast.net>
+ Web: <https://avpodcast.net/podcastlinux>
+ Blog: <https://podcastlinux.gitlab.io/>


Si te interesa estar al tanto de todo lo que hago, acá abajo estan las redes en las que
puedes **contactarme**:


+ Twitter: <https://twitter.com/razon_voz>
+ Correo: <larazondelavoz@disroot.org>
+ Web: <https://larazondelavoz.gitlab.io/>
+ Telegram: <https://t.me/larazondelavoz>
+ Feed del Podcast: <https://larazondelavoz.gitlab.io/feed>
+ Feed del podcast por FeedBurner: <http://feeds.feedburner.com/larazondelavoz>
+ Mastodon: <https://mastodon.social/@larazondelavoz1>