---
layout: post  
title: "Podcast #04 Entrevista a ex-miembro desarrollador de Canaima "  
date: 2018-04-30  
categories: podcast  
image: images/portadapod04.png  
podcast_link: https://archive.org/download/pod04entre/pod04
tags: [audio, música, La Raón de La Voz, larazondelavoz, software libre]  
comments: true 
---

¡Hey! Hola, el día de hoy te traigo una entrevista con un antiguo miembro (y uno de los principales responsables en su momento) del equipo encargado de desarrollar Canaima hasta la versión 4.1; espero que de verdad lo disfruten.  


<audio controls>
  <source src="https://archive.org/download/pod04entre/pod04.ogg" type="audio/ogg">
  <source src="https://archive.org/download/pod04entre/pod04.mp3" type="audio/mpeg">
</audio>
![#portadapod04](https://larazondelavoz.gitlab.io/images/portadapod04.png)



¡Descubre **Canaima**!:
+ Web: <https://canaima.softwarelibre.gob.ve>

¡**Contacta** con Luis Alejandro Martínez! :
+ Telegram: @LuisAlejandro
+ Youtube: <https://www.youtube.com/channel/UCB1YnqZ98XGzLeLYFeuib_A>
+ Twitter: <https://twitter.com/LuisAlejandro>
+ Web: <http://huntingbears.com.ve/>
+ Video 1: <https://www.youtube.com/watch?v=6juFDuJ8VaA>
+ Video 2: <https://www.youtube.com/watch?v=l62fkWsEOvE>



Yo soy La Razón y La Voz y te agradezco haberte animado a escuchar, aunque fuese por un momento este podcast, ¡Gracias!

Si te interesa estar al tanto de todo lo que hago, acá abajo estan las redes en las que
puedes **encontrarme**:


+ [Feed del podcast](https://larazondelavoz.gitlab.io/feed) 
+ [Feed del Podcast con FeedBurner](http://feeds.feedburner.com/larazondelavoz)  
+ [Twitter](https://twitter.com/razon_voz)  
+ [Web](https://larazondelavoz.gitlab.io/)  
+ [Correo](larazondelavoz@disroot.org)  
+ [Telegram](https://t.me/larazondelavoz)  
+ [Mastodon](https://mastodon.social/@larazondelavoz1)  

Las canciones que escuchaste durante el podcast:  
**boxboys - disability**  
**Tom Misch - The Real Thing**  
**Tom Misch - In A Special Way**  
**Tom Misch - SummerTom Misch - Where Were You**  
**Tom Misch - Keep MovingTom Misch - Lush Life**  
**Tom Misch - EpiphanyTom Misch - Dilla LoveTom Misch - Cruisin'**  
**Tom Misch - DeeperTom Misch - Windmills Of Your Mind**  
**Tom Misch - Ahmad Heavy ShortTom Misch - Climbing**  
**Tom Misch - Welcome To My UniverseTom Misch - Euphoric**  
**Tom Misch - Can't Explain ItTom Misch - Maguel Chops**  
**Tom Misch - Wind (Jazzy Joint)Tom Misch - Wonder**  
**Tom Misch - You Got Me Flying**  
**Lobo Loco #04 - Happy Forbidden District (ID:895)**  
**Lobo Loco #08 - Shorty The Chicken (ID:867)**  
**Green A - Carta a Dios**  
La música empleada en este podcast cumple con todos los derechos de autor y aquí tuvieron su debido reconocimiento.



+ Licencia: <http://creativecommons.org/licenses/by-sa/4.0/> 


