---
layout: post  
title: "Podcast #21 Noticias del mundo Gaming en Linux, Recomendación de juegos"  
date: 2018-08-06  
categories: podcast  
image: images/imagen2.png  
podcast_link: https://archive.org/download/pod21gaming/pod21  
tags: [audio, música, vlog, La Razón de La Voz, larazondelavoz, reconmendación, juegos, podcast, gaming]  
comments: true  
---

¡Hey! Hola ¿Qué tal? ¿Cómo estas? Espero que te encuentres muy bien.  Hoy te traigo las noticias que a mi parecer son las mas interesantes, relevantes o curiosas referente a lo que viene siendo el gaming en linux, para ti que en ocasiones tienes ratos libres y no sabes como aprovecharlos, no te apetece leer, te traigo esta sección, además de que te recomendare un par de juegos bastante entretenidos de los que yo disfruto mucho y se que son productos de calidad.  

<audio controls>
  <source src="https://archive.org/download/pod21gaming/pod21.ogg" type="audio/ogg">
  <source src="https://archive.org/download/pod21gaming/pod21.mp3" type="audio/mpeg">
</audio>
![#portadapod21](https://larazondelavoz.gitlab.io/images/portadapod21.png)  

Webs de noticias:  
https://www.phoronix.com/scan.php?page=news_item&px=Xenko-3.0-Released  
http://linuxgamenews.com/

[TangleWood](https://tanglewoodgame.com/index.html)


¡Descubre una biblioteca muy liviana con [El Perro y La Rana](http://www.elperroylarana.gob.ve/biblioteca-mas-liviana-del-mundo/) .


¡Algunos blogs que quizá te puendan interesar*  

+ Dale un vistazo a [El Blog de Lazaro.](https://elblogdelazaro.gitlab.io/) (un blog muy. muy interesante).  
+ Escucha a Juan Febles de [Podcast Linux.](https://podcastlinux.gitlab.io)(El blog/podcast de la persona gracias a la cual pude crear este podcast).  
+ ¡Escucha el podcast de [Papa Friki!](http://papafriki.gitlab.io/podcast/) .



Yo soy La Razón y La Voz y te agradezco haberte animado a escuchar, aunque fuese por un momento este podcast, ¡Gracias!

Si te interesa estar al tanto de todo lo que hago, acá abajo estan las redes en las que
puedes **encontrarme**:  
 

+ [Feed del podcast](https://larazondelavoz.gitlab.io/feed)
+ [Feed del podcas con FeedBurner](http://feeds.feedburner.com/larazondelavoz)
+ [Twitter](https://twitter.com/razon_voz)
+ [Web](https://larazondelavoz.gitlab.io/)  
+ [Correo](larazondelavoz@disroot.org)  
+ [Telegram](https://t.me/larazondelavoz)  
+ [Mastodon](https://mastodon.social/@larazondelavoz1)  


Las canciones que escuchaste durante el podcast:  
**Tobu – Hope**  
**Intro Tobu – Cloud 9**  
**Electro Ligth – Symbolism**  
**Tym – Talk With Me**  
**Artesia - Her**  
**Green A – Carta a Dios**  

La música empleada en este podcast cumple con todos los derechos de autor y aquí tuvieron su debido reconocimiento.


+ Licencia: <http://creativecommons.org/licenses/by-sa/4.0/> 