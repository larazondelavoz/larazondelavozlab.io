---
layout: post  
title: "Ocaso de sangriento"  
date: 2018-10-09  
categories: articulo  
image: images/portadapod26.png  
tags: [lectura, escrito, sobrenatural]  
comments: true  
---


![#portadapod26](https://larazondelavoz.gitlab.io/images/estatua.jpg)  


Y allí ocurre el ocaso sangriento, una curiosa época del año en el que cierto lugar de la ciudad se tiñe de un marrón rojizo intenso, peculiar, extraño poco propio de la naturaleza, como sangre reseca por una larga exposición al aire. Aquí se reúnen pues las 7 ordenes de los antiguos, seres sabios que existen en la oscuridad del mundo, seres sabios que han existido desde siempre, que evalúan si es pertinente o no brindarle sus conocimientos a la humanidad, siempre terminan con la misma conclusión sus reuniones "no vale la pena otorgarles conocimientos que usaran para destruirse", ellos se dieron cuenta de que cada intervención que tuvieron con los humanos termino en tragedia, cada ayuda terminaba empeorando las cosas, por ahora la humanidad no es más que ganado para ellos, un alimento que se produce en masa, no pueden evitarlo, pese a que no es tan necesario para ellos el hacerlo, no necesitan demasiado de los humanos, de su carne, de su sangre, para mantenerse sanos, fuertes; "deberíamos reducir su población" sentencio uno de los más jóvenes, tenia a penas unos pocos milenios encima pero hablaba con sabiduría, las guerras que hacen entre ellos por estupideces suele hacerlo, aun así el joven tiene razón, su población ha crecido demasiado y una guerra actual con las armas que poseen podría acabar con todo "los humanos son una especie de presa que se volvió depredadora y no perdió los hábitos propios de un animal de presa" dijo con contundencia el más anciano, luego de esas palabras sólo se escucharon murmullos, susurros en toda la gran sala, tratando de acordar como lograrían acabar con un 60% de la población global sin causar una guerra. Mientras tanto sobre ellos se cierne la noche, el sol estaba dando unos últimos destellos de rojizo intenso sobre la estatua central de la plaza y un niño que paseaba con su madre por allí le aseguraba "la estatua 
llora sangre, madre".   