---
layout: post  
title: "Podcast #14 ¡Una tarde con Papa Friki!"  
date: 2018-07-06  
categories: podcast  
image: images/portadapod14.png  
podcast_link: https://archive.org/download/pod14papafriki/pod14
tags: [audio, música, entrevista, vlog, La Razón de La Voz, larazondelavoz, día del padre, software libre, nostalgia]  
comments: true 
---

¡Hola! ¿Qué tal? Espero que estes muy bien, yo me encuentro súper emocionado por traerte a un invitado acá a este humilde riconcillo. Papá Friki es un podcaster que suele tratar temas de su día a día, sus experiencias cacharreando y demás, sin lugar a dudas alguien que realmente merece la pena escuchar.  

<audio controls>
  <source src="https://archive.org/download/pod14papafriki/pod14.ogg" type="audio/ogg">
  <source src="https://archive.org/download/pod14papafriki/pod14.mp3" type="audio/mpeg">
</audio>
![#portadapod14](https://larazondelavoz.gitlab.io/images/portadapod14.png)



¡Descubre una biblioteca muy liviana con [El Perro y La Rana](http://www.elperroylarana.gob.ve/biblioteca-mas-liviana-del-mundo/) .


¡Algunos blogs que quizá te puendan interesar*  

+ Dale un vistazo a [El Blog de Lazaro.](https://elblogdelazaro.gitlab.io/) (un blog muy. muy interesante).  
+ Escucha a Juan Febles de [Podcast Linux.](https://podcastlinux.gitlab.io)(El blog/podcast de la persona gracias a la cual pude crear este podcast).  
+ ¡Escucha el podcast de [Papa Friki!](http://papafriki.gitlab.io/podcast/) .



Yo soy La Razón y La Voz y te agradezco haberte animado a escuchar, aunque fuese por un momento este podcast, ¡Gracias!

Si te interesa estar al tanto de todo lo que hago, acá abajo estan las redes en las que
puedes **encontrarme**:


+ [Feed del podcast](https://larazondelavoz.gitlab.io/feed)
+ [Feed del podcas con FeedBurner](http://feeds.feedburner.com/larazondelavoz)
+ [Twitter](https://twitter.com/razon_voz)
+ [Web](https://larazondelavoz.gitlab.io/)  
+ [Correo](larazondelavoz@disroot.org)  
+ [Telegram](https://t.me/larazondelavoz)  
+ [Mastodon](https://mastodon.social/@larazondelavoz1)  

Las canciones que escuchaste durante el podcast:  
**Loquillo - Rock & Roll Star**  
**The Neverending Story- Atreju Meets Falkor**  
**The Neverending Story- Gmork Moonchild**  
**The Neverending Story- Mirrorgate- Southern Oracle**  
**The Neverending Story- The Auryn  Happy Flight**
**Loquillo - Rompe olas**

La música empleada en este podcast cumple con todos los derechos de autor y aquí tuvieron su debido reconocimiento.



+ Licencia: <http://creativecommons.org/licenses/by-sa/4.0/> 