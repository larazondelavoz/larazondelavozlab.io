---
layout: post  
title: "Podcast #07 Experiencias en Ubuntu y Fedora, juegos que me llamaron, rapero Cevlade"  
date: 2018-05-15  
categories: podcast  
image: images/portadapod07.png  
podcast_link: https://archive.org/download/pod07/pod07
tags: [audio, música, La Razón de La Voz, larazondelavoz, software libre]  
comments: true 
---

¡Hey! Hola, te voy a hablar en esta ocasión de mis experiencias en Ubuntu y Fedora, además de algunos juegos curiosos y muy entretenidos con los que me tope en la store, y para cerrar te presentare a Cevlade un gran rapero chileno.  

<audio controls>
  <source src="https://archive.org/download/pod07/pod07.ogg" type="audio/ogg">
  <source src="https://archive.org/download/pod07/pod07.mp3" type="audio/mpeg">
</audio>
![#portadapod07](https://larazondelavoz.gitlab.io/images/portadapod07.png)


¡Descubre a **Cevlade**!:
+ Web: <http://www.cevlade.com>

¡Algunos blogs que quizá te puendan interesar*  

+ EL Blog de Lazaro (un blog muy. muy interesante): <https://elblogdelazaro.gitlab.io/>
+ Podcast Linux: <https://podcastlinux.gitlab.io>





Yo soy La Razón y La Voz y te agradezco haberte animado a escuchar, aunque fuese por un momento este podcast, ¡Gracias!

Si te interesa estar al tanto de todo lo que hago, acá abajo estan las redes en las que
puedes **encontrarme**:


+ [Feed del podcast](https://larazondelavoz.gitlab.io/feed) 
+ [Feed del Podcast con FeedBurner](http://feeds.feedburner.com/larazondelavoz)  
+ [Twitter](https://twitter.com/razon_voz)  
+ [Web](https://larazondelavoz.gitlab.io/)  
+ [Correo](larazondelavoz@disroot.org)  
+ [Telegram](https://t.me/larazondelavoz)  
+ [Mastodon](https://mastodon.social/@larazondelavoz1)  

Las canciones que escuchaste durante el podcast:  
**SURF - 6am**  
**LAKEY INSPIRED - Betters Days**  
**Lobo Loco - 04 - Happy Forbidden District (ID:895)**  
**tym - walk with me**  
**Cevlade - La Panacea**  
**LAKEY INSPIRED - That Girl **  
**Cevladé - Las lágrimas no ayudan**  
La música empleada en este podcast cumple con todos los derechos de autor y aquí tuvieron su debido reconocimiento.



+ Licencia: <http://creativecommons.org/licenses/by-sa/4.0/> 


