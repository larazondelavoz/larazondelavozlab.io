---  
layout: post  
title: "Podcast #52 ¿Una hora libre?"  
date: 2019-12-02  
categories: podcast  
image: images/image.jpg  
podcast_link: https://archive.org/download/unahoralibre003/%C2%BFUna%20hora%20libre  
tags: [audio, La Razón de La Voz, podcast, trabajo]  
comments: true  
---  


Como ya vengo acostumbradolos voy a quejarme de muchas cosas, en especial de mi trabajo, mejor dicho de mis jefes, y contar un par de cositas varias que me ponen contento.  





<audio controls>  
  <source src="https://archive.org/download/unahoralibre003/%C2%BFUna%20hora%20libre.ogg" type="audio/ogg">  
  <source src="https://archive.org/download/unahoralibre003/%C2%BFUna%20hora%20libre.mp3" type="audio/mpeg">  
</audio>  



[¿Puedes ayudar a crear un internet más libre, segura, mejor?](https://donate.mozilla.org/es-MX/?utm_source=newsletter-mofo&utm_medium=email&utm_campaign=nov27em-es&utm_content=calloutbutton2&utm_term=4928856).    
 



Lee mi libro [Al Borde del Abismo](https://litnet.com/es/book/al-borde-del-abismo-b87851#) y si puedes deja tu me gusta si fue de tu agrado, de lo contrario comentame porque te disgustó, y si lo compartes me ayudaría aun más.    


¡Webs de juegos retro!:  
[1985 Alertenativo](http://fasebonus.net/1985alternativo/)  
[MojonTwins](http://www.mojontwins.com/juegos_mojonos/)  
[Kabuto Factori](http://kabutofactory.altervista.org/index.html)  
[PDRoms](https://pdroms.de/)  
[FreeRoms](https://www.freeroms.com/roms/gameboy_color/pdroms_com_re-opening_demo_public_domain.htm) (mi web favorita)  
[FreeVintageGames](http://freevintagegames.com)  

¡Descubre una biblioteca muy liviana con [El Perro y La Rana](http://www.elperroylarana.gob.ve/biblioteca-mas-liviana-del-mundo/) .  


¡Algunos blogs que quizá te puendan interesar*    

+ Dale un vistazo a [El Blog de Lazaro.](https://elblogdelazaro.gitlab.io/) (un blog muy. muy interesante).  
+ Escucha a Juan Febles de [Podcast Linux.](https://podcastlinux.gitlab.io)(El blog/podcast de la persona gracias a la cual pude crear este podcast).  
+ ¡Escucha el podcast de [Papa Friki!](http://papafriki.gitlab.io/podcast/) .  



Yo soy La Razón y La Voz y te agradezco haberte animado a escuchar, aunque fuese por un momento este podcast, ¡Gracias!  

Si te interesa estar al tanto de todo lo que hago, acá abajo estan las redes en las que
puedes **encontrarme**:  


+ [Feed del podcast](https://larazondelavoz.gitlab.io/feed)  
+ [Feed del podcas con FeedBurner](http://feeds.feedburner.com/larazondelavoz)  
+ [Twitter](https://twitter.com/razon_voz)  
+ [Web](https://larazondelavoz.gitlab.io/)  
+ [Correo](larazondelavoz@disroot.org)  
+ [Telegram](https://t.me/larazondelavoz)  
+ [Mastodon](https://mastodon.social/@larazondelavoz1)   

 
+ Licencia: <http://creativecommons.org/licenses/by-sa/4.0/>  